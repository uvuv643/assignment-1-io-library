section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length: ; works
    .loop:
        xor rax, rax
    .count:
        test byte[rdi + rax], 0xFF
        je .end
        inc rax
        jmp .count
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; works
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret


print_newline: ; works
    mov rdi, 0xA
print_char: ; works
    push rdi
    mov rsi, rsp ; получить адрес символа
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int: ; works
    test rdi, rdi
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

print_uint: ; works
    mov rax, rdi
    mov r8, 10
    push 0x0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, 0x30
        push rdx
        test rax, 0xFF
        jne .loop

    add rax, 0x30
    cmp rax, 0x30
    je .print_next
    push rax

    .print_next:
        pop rax
        test rax, 0xFF
        je .end
        mov rdi, rax
        call print_char
        jmp .print_next

    .end:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rcx, 0

    .next_symbol:
        mov al, byte[rdi + rcx]
        mov ah, byte[rsi + rcx]
        test al, 0xFF
        je .zero_first
        test ah, 0xFF
        je .zero_second
        inc rcx
        jmp .next_symbol

    .zero_first:
        test ah, 0xFF
        je .equals
        jmp .notequals

    .zero_second:
        test al, 0xFF
        je .equals
        jmp .notequals

    .equals:
        mov rax, 1
        ret
    .notequals:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi ; начало буффера
    mov r9, rsi ; длина буффера
    mov r10, rdi ; answer
    sub r9, 1 ; \0

    .skip_symbols:
        call read_char
        cmp rax, 0x20
        je .skip_symbols
        cmp rax, 0x9
        je .skip_symbols
        cmp rax, 0xA
        je .skip_symbols

    .read_symbols:
        cmp rax, 0
        je .create_word
        cmp rax, 0x20
        je .create_word
        cmp rax, 0x9
        je .create_word
        cmp rax, 0xA
        je .error

        mov [r8], rax
        add r8, 1
        sub r9, 1
        cmp r9, 1
        jb .error

        call read_char

        jmp .read_symbols

    .create_word:
        mov byte[r8], 0
        mov rdi, r10
        call string_length
        mov rdx, rax
        mov rax, r10
        ret

    .error:
        xor rax, rax
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        cmp byte[rdi + rdx], 0x0
        je .exit
        cmp byte[rdi + rdx], 0x39
        jg .exit
        mov cl, byte[rdi + rdx]
        sub cl, 0x30
        imul rax, 10
        add rax, rcx
        inc rdx
        jmp .loop
    .exit:
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    jmp .exit
    .neg:
        add rdi, 1
        call parse_uint
        neg rax
        add rdx, 1
    .exit:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        je .error
        test byte[rdi + rax], 0xFF
        je .exit
        mov cl, [rdi + rax]
        mov byte[rsi + rax], cl
        inc rax
        jmp .loop
    .error:
        xor rax, rax
        ret
    .exit:
        mov byte[rsi + rax], 0
        ret